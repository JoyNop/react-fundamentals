import * as React from 'react'


export interface PlayerInfo {

    avatar: string
    username: string
}
export class PlayerPreview extends React.Component<PlayerInfo> {
    public render() {
        return (
            <div>
                <div className='column'>
                    <img
                        className='avatar'
                        src={this.props.avatar}
                        alt={"Avatar for"+this.props.username}

                    />
                    <h2 className='username'>@{this.props.username}</h2>
                </div>
                {this.props.children}
            </div>
        )
    }


}