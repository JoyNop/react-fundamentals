import * as React from 'react';
 import './App.css';
// import * as ReactRouter from 'react-router-dom'
import { Home } from './Home'
import { Nav } from './Nav'
import { Battle } from './Battle'
import {Popular} from './Popular'
import {Results} from './Results'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

export class App extends React.Component {
   render() {
    return (
      <Router>
        <div className='container'>
          <Nav />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/battle' component={Battle} />
            <Route path='/battle/results' component={Results} />
            <Route path='/popular' component={Popular} />

            <Route render={function () {
              return <p>Not Found</p>
            }} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
